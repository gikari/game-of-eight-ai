#include "model.h"
#include <QDebug>
#include <QVector>
#include <vector>

namespace GameOfEight {

Model::Model(QObject *parent) : QObject(parent),
    currentDepth{0},
    currentMaxDepth{-1},
    pseudoEndingStates{},
    pseudoEndingStatesDepths{},
    statesToAnalyzeDepths{},
    currentState{
        {8, 7, 3},
        {1, 5, 6},
        {4, 2, 0}
        },
    visitedStates{},
    statesToAnalyze{},
    breadthSearchTotalMoves{},
    breadthSearchMaxQueueSize{},
    depthSearchTotalMoves{},
    depthSearchMaxQueueSize{}
{

}

void Model::solve()
{
    reset();
    qDebug() << "Breadth search began!";
    solveWithBreadthSearch();
    qDebug() << "Breadth search ended!";

    qDebug() << "\n\n\n\n\n\n";

    reset();
    qDebug() << "Incremental Depth search began!";
    solveWithIncrementDepthSearch();
    qDebug() << "Incremental Depth search ended!";

    qDebug() << "Breadth search total moves: " << breadthSearchTotalMoves;
    qDebug() << "Breadth search max queue size: " << breadthSearchMaxQueueSize;
    qDebug() << "Incremental Depth search total moves: " << depthSearchTotalMoves;
    qDebug() << "Incremental Depth search max queue size: " << depthSearchMaxQueueSize;
}

void Model::reset()
{
    currentState ={
        {8, 7, 3},
        {1, 5, 6},
        {4, 2, 0}
    };
    visitedStates.clear();
    statesToAnalyze.clear();
}

bool Model::isRepeatedState(GameState state)
{
    return visitedStates.contains(state);
}

char Model::getCellChar(int row, int column)
{
    int cellValue {-1};
    try {
        cellValue = currentState[row][column];
        return cellValue == 0 ? ' ' : QString::number(cellValue).toStdString()[0];
    } catch (std::exception) {
        return 'X';
    }
}

void Model::printCurrentState()
{
    qDebug() << "Current state:\n\n"
             << getCellChar(0, 0) << "|" << getCellChar(0, 1) << "|" << getCellChar(0, 2) << "\n"
             << "--+---+--\n"
             << getCellChar(1, 0) << "|" << getCellChar(1, 1) << "|" << getCellChar(1, 2) << "\n"
             << "--+---+--\n"
             << getCellChar(2, 0) << "|" << getCellChar(2, 1) << "|" << getCellChar(2, 2) << "\n" ;
}

void Model::solveWithBreadthSearch()
{
    breadthSearchTotalMoves++;
    if (statesToAnalyze.size() > breadthSearchMaxQueueSize) {
        breadthSearchMaxQueueSize = statesToAnalyze.size();
    }

    qDebug() << "\n============================";
    printCurrentState();
    if (isWin()) {
       qDebug() << "Game finished! Victory!";
       qDebug() << "============================";
    } else {
        if (!isRepeatedState(currentState)) {
           visitedStates += currentState;
           statesToAnalyze += getPossibleFutureStates();
        } else {
            qDebug() << "This was a repeated state!";
        }
        currentState = statesToAnalyze[0];
        statesToAnalyze.pop_front();
        qDebug() << "============================";
        solveWithBreadthSearch();
    }
}

void Model::solveWithIncrementDepthSearch()
{
    depthSearchTotalMoves++;
    if (statesToAnalyze.size() > depthSearchMaxQueueSize) {
        depthSearchMaxQueueSize = statesToAnalyze.size();
    }

    qDebug() << "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
    printCurrentState();
    if (isWin()) {
        qDebug() << "Game finished! Victory!";
        qDebug() << "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
    } else {
        if (isRepeatedState(currentState)) {
            qDebug() << "This was a repeated state!";
        } else if (currentDepth > currentMaxDepth) {
            pseudoEndingStates += currentState;
            pseudoEndingStatesDepths += currentDepth;
            qDebug() << "This was a too deep state!";
        } else {
            visitedStates += currentState;
            QVector<GameState> possibleStates = getPossibleFutureStates();
            statesToAnalyze = possibleStates + statesToAnalyze;

            int deeperDepth = currentDepth + 1;
            for (int i = 0; i < possibleStates.size(); i++) {
                statesToAnalyzeDepths.prepend(deeperDepth);
            }
        }

        if (statesToAnalyze.isEmpty()) {
            qDebug() << "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
            qDebug() << "Max depth reached! Increasing the limit!";
            currentMaxDepth++;
            statesToAnalyze = pseudoEndingStates;
            statesToAnalyzeDepths = pseudoEndingStatesDepths;
            pseudoEndingStates.clear();
            pseudoEndingStatesDepths.clear();
        } else {
            currentState = statesToAnalyze[0];
            currentDepth = statesToAnalyzeDepths[0];
            statesToAnalyzeDepths.pop_front();
            statesToAnalyze.pop_front();
        }
        qDebug() << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
        solveWithIncrementDepthSearch();
    }
}

QVector<GameState> Model::getPossibleFutureStates()
{
    QVector<GameState> futureStates {};

    CellAddress zeroCellCoordinates = getZeroCellAddress();
    QVector<Direction> directions {Direction::UP, Direction::RIGHT, Direction::DOWN, Direction::LEFT};
    for (Direction direction : directions) {
        if (getNeighboor(zeroCellCoordinates, direction) != -1) {
            futureStates += makeMoveInCurrentState(direction);
        }
    }
    return futureStates;
}

bool Model::isWin()
{
    return currentState[0][0] == 1
        && currentState[0][1] == 2
        && currentState[0][2] == 3
        && currentState[1][0] == 4
        && currentState[1][1] == 5
        && currentState[1][2] == 6
        && currentState[2][0] == 7
            && currentState[2][1] == 8;
}

CellAddress Model::getZeroCellAddress()
{
    for (int i = 0; i < currentState.size(); i++) {
        for (int j = 0; j < currentState[i].size(); j++) {
            if (currentState[i][j] == 0) {
                return {i, j};
            }
        }
    }
    return {-1, -1};
}

CellValue Model::getNeighboor(CellAddress cellAddress, Model::Direction direction)
{
    CellAddress neighboorAddress {getNeighboorAddress(cellAddress, direction)};
    if (neighboorAddress.first < 3 && neighboorAddress.first > -1
            && neighboorAddress.second < 3 && neighboorAddress.second > -1) {
        return currentState[neighboorAddress.first][neighboorAddress.second];
    } else {
        return -1;
    }
}

CellAddress Model::getNeighboorAddress(CellAddress cellAddress, Model::Direction direction)
{
    int rowNumber = cellAddress.first;
    int columnNumber = cellAddress.second;
    switch (direction) {
        case Direction::UP:
            return {rowNumber - 1, columnNumber};
        case Direction::RIGHT:
            return {rowNumber, columnNumber + 1};
        case Direction::DOWN:
            return {rowNumber + 1, columnNumber};
        case Direction::LEFT:
            return {rowNumber, columnNumber - 1};
    }
    return {-1, -1};
}

GameState Model::makeMoveInCurrentState(Model::Direction direction)
{
    CellAddress zeroCellAddress = getZeroCellAddress();
    int neighboorValue {getNeighboor(zeroCellAddress, direction)};
    if (neighboorValue == -1) {
        return {};
    } else {
        GameState futureState = currentState;
        CellAddress neighboorAddress = getNeighboorAddress(zeroCellAddress, direction);
        int tmp = futureState[zeroCellAddress.first][zeroCellAddress.second];
        futureState[zeroCellAddress.first][zeroCellAddress.second] = neighboorValue;
        futureState[neighboorAddress.first][neighboorAddress.second] = tmp;
        return futureState;
    }
}

}
