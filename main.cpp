#include "model.h"

int main(int, char**)
{
    GameOfEight::Model model {};
    model.solve();
    return 0;
}
