#pragma once

#include <QObject>
#include <QVector>
#include <QSet>

#include <vector>

namespace GameOfEight {

using CellAddress = std::pair<int, int>;
using CellValue = int;
using GameState = QVector<QVector<int>>;
//using GameState = std::vector<std::vector<int>>;

//class GameState : public QVector<QVector<int>> {
//    using QVector::QVector;
//};

//class GameState : public QVector<QVector<int>> {
//    using QVector::QVector;
//public:
//    int depth;

//};

class Model : public QObject
{
    Q_OBJECT
public:
    enum class Direction {UP, RIGHT, DOWN, LEFT};
    explicit Model(QObject *parent = nullptr);

public:
    void solve();
    void reset();

private:
    void solveWithBreadthSearch();
    void solveWithIncrementDepthSearch();

    void printCurrentState();

    bool isWin();
    bool isRepeatedState(GameState state);

    char getCellChar(int row, int column);

    CellAddress getZeroCellAddress();
    QVector<GameState> getPossibleFutureStates();

    CellValue getNeighboor(CellAddress cellAddress, Direction direction);
    CellAddress getNeighboorAddress(CellAddress cellAddress, Direction direction);
    GameState makeMoveInCurrentState(Direction direction);

    int currentDepth;
    int currentMaxDepth;
    QVector<GameState> pseudoEndingStates;
    QVector<int> pseudoEndingStatesDepths;
    QVector<int> statesToAnalyzeDepths;

    GameState currentState;
    QSet<GameState> visitedStates;
    QVector<GameState> statesToAnalyze;

    int breadthSearchTotalMoves;
    int breadthSearchMaxQueueSize;
    int depthSearchTotalMoves;
    int depthSearchMaxQueueSize;
};

}
